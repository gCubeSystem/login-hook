
# Changelog for Login Hook

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.5.0] - 2021-02-25

- Ported to git

- Disabled the possibility to login via Liferay. Shows a Button pointing to keycloak login if a user is redirected there by old pages. 

## [v1.4.0] - 2018-10-25

Restyled login page

## [v1.2.0] - 2017-02-06

Implemented support for oAuth 2

## [v1.0.0] - 2016-03-30

Login hook for liferay login portlet. Offers a more attractive social login UI
